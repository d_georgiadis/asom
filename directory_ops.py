import os


def get_list_of_files(dirName, suffix=None):
    '''
    For the given path, get the List of all file paths in the directory tree.
    If a suffix is given, then only filepaths with a specified suffix.
    '''
    # create a list of file and sub directories
    # names in the given directory
    listOfFile = os.listdir(dirName)
    allFiles = list()

    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)

        # If entry is a directory then get the list of files in this directory
        if os.path.isdir(fullPath):
            allFiles = allFiles + get_list_of_files(fullPath)
        else:
            allFiles.append(fullPath)

    if suffix is None:
        return allFiles
    else:
        return [f for f in allFiles if f.endswith(suffix)]
