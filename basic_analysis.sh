#!/bin/bash

# Executes basic analysis of an SOM
# Sample usage:
#    bash basic_analysis.sh <path to the SOM directory> <fsilename of the som> <path to the train dataset> <path to the test dataset> <path to the file with the feature names of the dataset>

# Dependencies:
# Uses the basic scientific python stack:
# - pandas, numpy, sys, os, scipy, json, sklearn, matplotlib, collections

# Make sure the following py files are in the same dir with this bash script:
# -som2df.py
# -classify_units.py
# -plot_umatrix.py
# -plot_target_map.py
# -plot_features_map.py
# -plot_som_as_images.py
# -assess_classifier.py
# -voronoi_plotter.py

process_one_file () {
  # Unpack
  SOM_DIR=$1
  SOM_FILENAME=$2
  TRAIN_PATH=$3
  TEST_PATH=$4
  FEATURE_NAMES_PATH=$5
  TASK_TYPE=$6

  # CORE ANALYSIS
  # Condition the map
  SOM_DF_PATH="${SOM_DIR}DF_${SOM_FILENAME}"
  SOM_PATH="${SOM_DIR}${SOM_FILENAME}"

  python som2df.py "${SOM_PATH}"  "${FEATURE_NAMES_PATH}"  "${SOM_DF_PATH}"

  # Classification report (confusion matrix, accuracy/recall/precision)
  if [ "$TASK_TYPE" == "classification" ];  then
    REPORT_PATH="${SOM_DIR}class_report_${SOM_FILENAME}.txt"
    python assess_classifier.py "${SOM_DF_PATH}"  "${FEATURE_NAMES_PATH}"  "${TEST_PATH}"  "${TRAIN_PATH}"  "${REPORT_PATH}"
  elif [ "$TASK_TYPE" == "regression" ];  then
   REPORT_PATH="${SOM_DIR}regression_report_${SOM_FILENAME}.txt"
   python assess_regressor.py "${SOM_DF_PATH}"  "${FEATURE_NAMES_PATH}"  "${TEST_PATH}"  "${TRAIN_PATH}"  "${REPORT_PATH}" 
  else
   echo "Error: bad TASK_TYPE input! %{TASK_TYPE}" > logfile.log
   exit 125
  fi

  # Plot basic perfomance metrics
  QERROR_PLOT_PATH="${SOM_DIR}_Qerror_${SOM_FILENAME}.png"
  python plot_unit_attribute.py "${SOM_DF_PATH}"  "${QERROR_PLOT_PATH}"  "Q_ERROR"

  TERROR_PLOT_PATH="${SOM_DIR}_Terror_${SOM_FILENAME}.png"
  python plot_unit_attribute.py "${SOM_DF_PATH}"  "${TERROR_PLOT_PATH}"  "BMU_DIST"

  NHITS_PLOT_PATH="${SOM_DIR}_Nhits_${SOM_FILENAME}.png"
  python plot_unit_attribute.py "${SOM_DF_PATH}"  "${NHITS_PLOT_PATH}"  "N_HITS"


  # OPTIONAL ANALYSIS

  # Plot the umatrix
  UMATRIX_PATH="${SOM_DIR}umatrix_${SOM_FILENAME}.png"
  python plot_umatrix.py "${SOM_DF_PATH}" "${FEATURE_NAMES_PATH}" "${UMATRIX_PATH}"

  # Plot the target matrix
  TARGET_MAP_PATH="${SOM_DIR}target_${SOM_FILENAME}.png"
  python plot_target_map.py "${SOM_DF_PATH}" "${TARGET_MAP_PATH}"

  # Feature map
  FEATURE_MAP_PATH="${SOM_DIR}featuremap_${SOM_FILENAME}.png"
  # TODO implemented feature map

  # Plot units as images
  IMAGE_MAP_PATH="${SOM_DIR}images_${SOM_FILENAME}.png"
#  python plot_som_as_images.py "${SOM_DF_PATH}" "${FEATURE_NAMES_PATH}" "${IMAGE_MAP_PATH}"
}

process_whole_dir () {
  # Unpack
  SOMS_DIR=$1
  TRAIN_PATH=$2
  TEST_PATH=$3
  FEATURE_NAMES_PATH=$4
  TASK_TYPE=$5

  # Iterate over the given directory and process all .json files
  for som_path in ${SOMS_DIR}*.json; do
    process_one_file  "${SOMS_DIR}"  "$(basename $som_path)"  "${TRAIN_PATH}"  "${TEST_PATH}" "${FEATURE_NAMES_PATH}" "${TASK_TYPE}"
  done
}

process_whole_dir "$@"