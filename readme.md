# TODO: adjust all methods, to only use their own axis when called from the terminal
# TODO: change the som2df method to store CSVs, not JSONs that are not JSONs.
# TODO: place all meta-parameters (e.g. the column labels ('ID' 'TARGET' 'I' 'J')) in a central configuration file
# TODO: implement SOM training
# TODO: move the training, unit labelling, and prediction functions to an sklearn compatible class, which will be called
# TODO: integrate 'target names', just like 'feature names' - this will make the classification results interpretable
