

def unit_attribute_plotter(som, att_name, output_path, dpi=100):
    import matplotlib.pyplot as plt
    import numpy as np

    # Get SOM strucutre
    map_width = int(som['J'].max() + 1)
    map_height = int(som['I'].max() + 1)  # +1 because it is an index

    # Transform to array
    arr = np.zeros((map_height, map_width)) * np.nan
    arr[som['I'].values.astype(int), som['J'].values.astype(int)] = som[att_name].values

    plt.figure(figsize=(10,10))
    plt.imshow(arr, cmap='plasma')
    plt.axis('off')
    plt.colorbar()
    plt.tight_layout()
    plt.savefig(output_path, dpi=dpi)


if __name__ == '__main__':
    import sys
    import numpy as np
    import pandas as pd

    som_path = sys.argv[1]
    output_path = sys.argv[2]
    att_name = sys.argv[3]

    som_df = pd.read_csv(som_path, index_col='ID')
    unit_attribute_plotter(som=som_df,
                           output_path=output_path,
                           att_name=att_name,
                           dpi=100)
