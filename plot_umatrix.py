

def get_spatial_discontinuity(som, feature_names):
    import numpy as np

    # Set norm for umatrix
    from scipy.linalg import norm as eucnorm
    norm = lambda x, y: eucnorm(x - y)

    # Wrapper for norm to handle nans
    def nannorm(x, y):
        any_nans = np.any(np.isnan(x)) or np.any(np.isnan(y))
        if any_nans:
            return np.nan
        else:
            return norm(x, y)

    # Get SOM strucutre
    map_width = int(som['J'].max() + 1)
    map_height = int(som['I'].max() + 1)  # +1 because it is an index
    n_features = len(feature_names)

    # Transform to array
    arr = np.zeros((map_height, map_width, n_features)) * np.nan
    arr[som['I'].values.astype(int), som['J'].values.astype(int), :] = som[feature_names].values

    # Get distances
    x, y, z = [], [], []
    for i in range(map_height - 1 ):
        for j in range(map_width - 1):
            x += [i]
            y += [j + 0.5]
            z += [nannorm(arr[i, j, :], arr[i, j + 1, :])]

            x += [i + 0.5]
            y += [j]
            z += [nannorm(arr[i, j, :], arr[i + 1, j, :])]

    return np.array(x), np.array(y), np.array(z)


def u_matrix_plotter(som, feature_names, max_z, dpi, output_path):
    import matplotlib.pyplot as plt
    from matplotlib import cm
    import numpy as np
    from voronoi_plotter import voronoi_plot

    # Get field of neuron distances (z)
    x, y, z = get_spatial_discontinuity(som, feature_names)
    aspect_ratio = (np.max(x) - np.min(x)) / (np.max(y) - np.min(y))

    # Normalise z
    if max_z is None: max_z = np.max(z)
    z = z / max_z

    # Plot the field using voronoi tesselation
    plt.figure(figsize=(8 * aspect_ratio, 8))
    plt.axis('off')
    voronoi_plot(-y, x, z,
                 ax=plt.gca(),
                 cmap=cm.plasma,
                 condition='infinite')
    plt.savefig(output_path, dpi=dpi)


if __name__ == '__main__':
    import sys
    import pandas as pd

    som_path = sys.argv[1]
    feature_names_path = sys.argv[2]
    output_path = sys.argv[3]

    som = pd.read_csv(som_path, index_col='ID')
    with open(feature_names_path, 'r') as f: feature_names = f.read().splitlines()

    u_matrix_plotter(som=som,
                     feature_names=feature_names,
                     max_z=None,
                     dpi=100,
                     output_path=output_path)

