

def feature_intensity_plotter(som, feature_id, feature_names, output_path,
                              dpi=100, logscale=True):
    from matplotlib.colors import LogNorm, Normalize
    import matplotlib.pyplot as plt
    from matplotlib import cm
    import numpy as np
    from collections.abc import Iterable

    # Get SOM strucutre
    map_width = int(som['J'].max() + 1)
    map_height = int(som['I'].max() + 1)  # +1 because it is an index
    n_features = len(feature_names)

    # Transform to array
    arr = np.zeros((map_height, map_width, n_features)) * np.nan
    arr[som['I'].values.astype(int), som['J'].values.astype(int), :] = som[feature_names].values

    # Plotting
    maxval = np.max(arr[:, :, feature_id])
    minval = np.min(arr[:, :, feature_id])
    minval = 0.0001
    unitlen = 3
    normer = (LogNorm if logscale else Normalize)
    if isinstance(feature_id, Iterable):
        N = len(feature_id)
        Nsq = int(np.ceil(np.sqrt(N)))
        fig, axarr = plt.subplots(Nsq, Nsq, figsize=(Nsq * unitlen, Nsq * unitlen))
        axvec = axarr.flatten()
        for i, fid in enumerate(feature_id):
            axvec[i].imshow(arr[:, :, fid],
                            cmap='plasma',
                            #    vmin=0, vmax=maxval,
                            norm=normer(vmin=minval, vmax=maxval))
            axvec[i].set_title(feature_names[fid])
            axvec[i].axis('off')
    else:
        plt.imshow(arr[:, :, feature_id],
                   cmap='plasma',
                   # vmin=0, vmax=maxval,
                   norm=normer(vmin=minval, vmax=maxval))
        plt.axis('off')

        # Save of display plots
    if output_path is None:
        plt.show()
    else:
        plt.savefig(output_path, dpi=dpi)


if __name__ == '__main__':
    import sys
    import numpy as np
    import pandas as pd

    som_path = sys.argv[1]
    feature_names_path = sys.argv[2]
    output_path = sys.argv[3]

    with open(feature_names_path, 'r') as f: feature_names = f.read().splitlines()
    som_df = pd.read_csv(som_path, index_col='ID')

    feature_intensity_plotter(som=som_df,
                              feature_id=np.arange(len(feature_names)).astype(int),
                              feature_names=feature_names,
                              output_path=output_path,
                              dpi=100,
                              logscale=True)

