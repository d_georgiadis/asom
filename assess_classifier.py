import torch
import numpy as np
import pandas as pd

from scipy.linalg import norm

DEVICE = 'cpu'

def label_units(som_df, data):
    target_name = 'TARGET'
    qerror_name = 'Q_ERROR'
    terror_name = 'BMU_DIST'
    nhits_name  = 'N_HITS'
    accuracy_name = 'ACCURACY'

    S = torch.Tensor(data[feature_names].values).to(DEVICE)
    W = torch.Tensor(som_df[feature_names].values).to(DEVICE)
    Tj = data[target_name].values

    # assign classes
    temp = som_df[target_name].values
    for i, w_i in enumerate(W):

        q_ij = (w_i - S).norm(dim=1)
        best_j = torch.argmin(q_ij).cpu().numpy()
        temp[i] = Tj[best_j]

    # Move to pandas
    som_df.loc[:, target_name] = temp

    
def assess_units(som_df, data):
    target_name = 'TARGET'
    qerror_name = 'Q_ERROR'
    terror_name = 'BMU_DIST'
    nhits_name  = 'N_HITS'
    accuracy_name = 'ACCURACY'
    chunksize = 1000

    # Get 
    # - Q error, 
    # - T error, 
    # - Number of hits per unit, 
    # - accuracy per unit, 
    # - precision per unit, 
    # - classificaiton report

    S = torch.Tensor(data[feature_names].values).to(DEVICE)
    W = torch.Tensor(som_df[feature_names].values).to(DEVICE)

    T = data[target_name].values
    Ti =som_df[target_name].values

    I = som_df.loc[:,'I'].values
    J = som_df.loc[:,'J'].values
    coor = np.stack([I,J]).T

    quant_i = np.zeros(len(som_df[qerror_name]))
    topo_i  = np.zeros(len(som_df[terror_name]))
    nhits_i = np.zeros(len(som_df[nhits_name]))
    acc_i = np.zeros(len(som_df[nhits_name]))
    y_pred = np.zeros(len(data[target_name]))

    #for j,s_j in enumerate(S):
    nchunks = int(len(S)/chunksize)
    for j,s_j in zip(torch.chunk(torch.Tensor(np.arange(len(S), dtype='int')),nchunks), 
                     torch.chunk(S,nchunks)):

        # Find the first and second BMUs
        q_ij = torch.cdist(s_j, W)
        best = torch.topk(q_ij, 2, largest=False, dim=1)

        bmu = best.indices.cpu().numpy()
        bmu1, bmu2 = bmu[:,0], bmu[:,1]

        # Are rpedictions accurate?
        j_np = j.cpu().numpy().astype(int)
        y_pred[j_np] = Ti[bmu1]
        correct_pred = T[j_np] == Ti[bmu1]

        u_dists = norm(coor[bmu1] - coor[bmu2], axis=1)>1#.norm() 

        for bmu1ii,bmu2ii,qii,predii,udistii in \
            zip(bmu1, bmu2, best.values[:,0].cpu().numpy(), correct_pred, u_dists): 
            # Increase the number of hits
            nhits_i[bmu1ii] += 1

            # Increase the quant error
            quant_i[bmu1ii] += qii

            # Increase the Topological error
            topo_i[bmu1ii] += udistii

            # Get accuracies
            acc_i[bmu1ii] += predii
        
        
    # Move to pandas
    som_df.loc[:, qerror_name] = quant_i/nhits_i
    som_df.loc[:, terror_name] = topo_i/nhits_i
    som_df.loc[:, nhits_name] = nhits_i
    som_df.loc[:, accuracy_name] = acc_i/nhits_i
            
    return y_pred
        
def predict(som_df, data):
    from sklearn.metrics import classification_report, confusion_matrix
    
    target_name = 'TARGET'
    qerror_name = 'Q_ERROR'
    terror_name = 'BMU_DIST'
    nhits_name  = 'N_HITS'
    accuracy_name = 'ACCURACY'
    
    S = torch.Tensor(data[feature_names].values).to(DEVICE)
    W = torch.Tensor(som_df[feature_names].values).to(DEVICE)
    Ti = som_df[target_name].values

    y_pred = np.ones(len(data[target_name]))

    for j,s_j in enumerate(S):

        # Find the first and second BMUs
        q_ij = (s_j - W).norm(dim=1)
        best_j = torch.argmin(q_ij).cpu().numpy()
        y_pred[j] = Ti[best_j]

    y_true = data[target_name]

    # Sklearn magic goes here
    class_rep = classification_report(y_true, y_pred, digits=3)
    conf_mat = confusion_matrix(y_true, y_pred)

    return class_rep, conf_mat
    

def assess_classifier(som_df, data):
    from sklearn.metrics import classification_report, confusion_matrix

    target_name = 'TARGET'
    qerror_name = 'Q_ERROR'
    terror_name = 'BMU_DIST'
    nhits_name  = 'N_HITS'
    accuracy_name = 'ACCURACY'

    # Predict classes
    label_units(som_df, data)
    y_pred = assess_units(som_df, data)
    y_true = data[target_name]

    # Sklearn magic goes here
    class_rep = classification_report(y_true, y_pred, digits=3)
    conf_mat = confusion_matrix(y_true, y_pred)

    return class_rep, conf_mat


if __name__  == "__main__":
    import sys
    import pandas as pd

    # Unpack
    som_path = sys.argv[1]
    feature_names_path = sys.argv[2]
    test_fn = sys.argv[3]
    train_fn = sys.argv[4]
    output_path = sys.argv[5]

    # Load data
    som = pd.read_csv(som_path, index_col='ID')
    with open(feature_names_path, 'r') as f: feature_names = f.read().splitlines()
    test = pd.read_csv(test_fn, index_col='ID')
    train = pd.read_csv(train_fn, index_col='ID')

    # Execute
    cr_train, cm_train = assess_classifier(som, train)
    som.to_csv(som_path)
    cr_test, cm_test = predict(som, test)

    # Save feature names to txt
    with open(output_path, 'w') as f:
        f.write("## CLASSIFICATIONS REPORT - IN SAMPLE##\n")
        for item in cr_train.split("\n"): f.write("%s\n" % item)

        f.write("\n ## CLASSIFICATIONS REPORT - OUT OF SAMPLE##\n")
        for item in cr_test.split("\n"): f.write("%s\n" % item)

        f.write("\n \n ## CONFUSION MATRIX - IN SAMPLE ##\n")
        for item in cm_train: f.write("%s\n" % item)

        f.write("\n ## CONFUSION MATRIX - OUT OF SAMPLE ##\n")
        for item in cm_test: f.write("%s\n" % item)

