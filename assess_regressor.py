import torch
import numpy as np
import pandas as pd

from scipy.linalg import norm


DEVICE='cpu'


def label_units(som_df, data):


    KNN_MEAN = 100

    target_name = 'TARGET'
    qerror_name = 'Q_ERROR'
    terror_name = 'BMU_DIST'
    nhits_name  = 'N_HITS'
    accuracy_name = 'MEAN_SQUARE_ERROR'

    S  = torch.Tensor(data[feature_names].values).to(DEVICE)
    W  = torch.Tensor(som_df[feature_names].values).to(DEVICE)
    Tj = torch.Tensor(data[target_name].values).to(DEVICE)

    # assign classes
    temp = som_df[target_name].values
    for i, w_i in enumerate(W):

        q_ij    = (w_i - S).norm(dim=1)
        BMSs    = torch.topk(-q_ij, KNN_MEAN).indices
        temp[i] = Tj[BMSs].mean().cpu().numpy()

    # Move to pandas
    som_df.loc[:, target_name] = temp

    
def assess_units(som_df, data):
    target_name = 'TARGET'
    qerror_name = 'Q_ERROR'
    terror_name = 'BMU_DIST'
    nhits_name  = 'N_HITS'
    accuracy_name = 'MEAN_SQUARE_ERROR'
    chunksize = 1000

    # Get 
    # - Q error, 
    # - T error, 
    # - Number of hits per unit, 
    # - accuracy per unit, 
    # - precision per unit, 
    # - classificaiton report

    S = torch.Tensor(data[feature_names].values).to(DEVICE)
    W = torch.Tensor(som_df[feature_names].values).to(DEVICE)

    T = data[target_name].values
    Ti =som_df[target_name].values

    I = som_df.loc[:,'I'].values
    J = som_df.loc[:,'J'].values
    coor = np.stack([I,J]).T

    quant_i = np.zeros(len(som_df[qerror_name]))
    topo_i  = np.zeros(len(som_df[terror_name]))
    nhits_i = np.zeros(len(som_df[nhits_name]))
    mse_i = np.zeros(len(som_df[nhits_name]))
    y_pred = np.zeros(len(data[target_name]))

    #for j,s_j in enumerate(S):
    nchunks = int(len(S)/chunksize)
    for j,s_j in zip(torch.chunk(torch.Tensor(np.arange(len(S), dtype='int')),nchunks), 
                     torch.chunk(S,nchunks)):

        # Find the first and second BMUs
        q_ij = torch.cdist(s_j, W)
        best = torch.topk(q_ij, 2, largest=False, dim=1)

        bmu = best.indices.cpu().numpy()
        bmu1, bmu2 = bmu[:,0], bmu[:,1]

        # Are rpedictions accurate?
        j_np = j.cpu().numpy().astype(int)
        y_pred[j_np] = Ti[bmu1]
        mean_sq_err = (T[j_np] - Ti[bmu1])**2

        u_dists = norm(coor[bmu1] - coor[bmu2], axis=1)>1#.norm() 

        for bmu1ii,bmu2ii,qii,predii,udistii in \
            zip(bmu1, bmu2, best.values[:,0].cpu().numpy(), mean_sq_err, u_dists): 
            # Increase the number of hits
            nhits_i[bmu1ii] += 1

            # Increase the quant error
            quant_i[bmu1ii] += qii

            # Increase the Topological error
            topo_i[bmu1ii] += udistii

            # Get accuracies
            mse_i[bmu1ii] += predii
        
        
    # Move to pandas
    som_df.loc[:, qerror_name] = quant_i/nhits_i
    som_df.loc[:, terror_name] = topo_i/nhits_i
    som_df.loc[:, nhits_name] = nhits_i
    som_df.loc[:, accuracy_name] = mse_i/nhits_i
            
    return y_pred
        
def predict(som_df, data):
    from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
    
    target_name = 'TARGET'
    qerror_name = 'Q_ERROR'
    terror_name = 'BMU_DIST'
    nhits_name  = 'N_HITS'
    accuracy_name = 'MEAN_SQUARE_ERROR'
    
    S = torch.Tensor(data[feature_names].values).to(DEVICE)
    W = torch.Tensor(som_df[feature_names].values).to(DEVICE)
    Ti = som_df[target_name].values

    y_pred = np.ones(len(data[target_name]))

    for j,s_j in enumerate(S):

        # Find the BMU
        q_ij = (s_j - W).norm(dim=1)
        best_j = torch.argmin(q_ij).cpu().numpy()
        y_pred[j] = Ti[best_j]

    y_true = data[target_name]

    # Sklearn magic goes here
    regression_metrics = {
     'mean_squared_error':  mean_squared_error(y_true, y_pred), 
     'r2_score':            r2_score(y_true, y_pred), 
     "mean_absolute_error": mean_absolute_error(y_true, y_pred)
     }
    return regression_metrics
    

def assess_regressor(som_df, data):
    from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error

    target_name = 'TARGET'
    qerror_name = 'Q_ERROR'
    terror_name = 'BMU_DIST'
    nhits_name  = 'N_HITS'
    accuracy_name = 'MEAN_SQUARE_ERROR'

    # Predict classes
    label_units(som_df, data)
    y_pred = assess_units(som_df, data)
    y_true = data[target_name]

    # Sklearn magic goes here
    regression_metrics = {
     'mean_squared_error':  mean_squared_error(y_true, y_pred), 
     'r2_score':            r2_score(y_true, y_pred), 
     "mean_absolute_error": mean_absolute_error(y_true, y_pred)
     }
    return regression_metrics


if __name__  == "__main__":
    import sys
    import pandas as pd

    # Unpack
    som_path = sys.argv[1]
    feature_names_path = sys.argv[2]
    test_fn = sys.argv[3]
    train_fn = sys.argv[4]
    output_path = sys.argv[5]

    # Load data
    som = pd.read_csv(som_path, index_col='ID')
    with open(feature_names_path, 'r') as f: feature_names = f.read().splitlines()
    test = pd.read_csv(test_fn, index_col='ID')
    train = pd.read_csv(train_fn, index_col='ID')

    # Execute
    reg_perf_train = assess_regressor(som, train)
    som.to_csv(som_path)
    reg_perf_test = predict(som, test)

    # Save feature names to txt
    with open(output_path, 'w') as f:
        f.write("## REGRESSION REPORT - IN SAMPLE##\n")
        for k in reg_perf_train: f.write(str(k)+" %s\n" % reg_perf_train[k])

        f.write("\n ## REGRESSION REPORT - OUT OF SAMPLE##\n")
        for k in reg_perf_test: f.write(str(k)+" %s\n" % reg_perf_test[k])
