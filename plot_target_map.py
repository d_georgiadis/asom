

def target_map_plotter(som, output_path, dpi=100):
    import matplotlib.pyplot as plt
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    import numpy as np

    # Get SOM strucutre
    map_width = int(som['J'].max() + 1)
    map_height = int(som['I'].max() + 1)  # +1 because it is an index

    # Transform to array
    arr = np.zeros((map_height, map_width)) * np.nan
    arr[som['I'].values.astype(int), som['J'].values.astype(int)] = som['TARGET'].values


    plt.figure(figsize=(10,10))
    ax=plt.gca()
    
    im = plt.imshow(arr, cmap='rainbow')
    plt.axis('off')

    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    plt.colorbar(im, cax=cax)

    plt.tight_layout()
    plt.savefig(output_path, dpi=dpi)


if __name__ == '__main__':
    import sys
    import numpy as np
    import pandas as pd

    som_path = sys.argv[1]
    output_path = sys.argv[2]

    som_df = pd.read_csv(som_path, index_col='ID')
    target_map_plotter(som=som_df,
                       output_path=output_path,
                       dpi=100)
