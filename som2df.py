

def load_unitmap(filename, feature_names):
    suffix = filename.split('.')[-1]
    if suffix == 'json':
        return _load_json_unitmap(filename, feature_names)
    elif suffix == 'csv':
        return _load_csv_unitmap(filename, feature_names)
    else:
        raise ('Unknown file format for the Unitmap..')


def _load_json_unitmap(path, feature_names):
    import json
    import pandas as pd
    import numpy as np
    # Read the map
    with open(path) as json_file:
        data = json.load(json_file)

    # Get map structure
    n_features = len(data['distributedUnits'][0]['unit']['dimensions'])
    n_units = len(data['distributedUnits'])

    extra_cols = ['I', 'J', 'TARGET', 'Q_ERROR', 'BMU_DIST', 'N_HITS', 'ACCURACY']

    # Make a dataframe
    temp = np.zeros((n_units, n_features + len(extra_cols)))
    for k, u_k in enumerate(data['distributedUnits']):
        temp[k, 0] = int(u_k['i'])
        temp[k, 1] = int(u_k['j'])
        temp[k, 2:2+len(extra_cols)-2] = np.nan
        temp[k, 2+len(extra_cols)-2:] = u_k['unit']['dimensions']

    print(temp.shape)
    print(len(extra_cols + feature_names))
    print('###############W')
    df = pd.DataFrame(temp, columns=extra_cols + feature_names)
    df.index.name = 'ID'
    return df


def _load_csv_unitmap(path, feature_names, map_is_square=True):
    import pandas as pd
    import numpy as np

    raw = pd.read_csv(path, header=None)

    # Remove the last columns if there are nans (sometimes the CSV wors have a "," in the end the pandas reads nans)
    if raw.iloc[:, -1].isna().any():
        raw = raw.iloc[:, :-1]

    # Get map structure
    n_units, n_features = raw.shape
    if map_is_square:
        height, width = np.sqrt(n_units).astype(int), np.sqrt(n_units).astype(int)
    else:
        raise("Can't guess the dimensions of the SOM!")

    # Make a DF
    temp = np.zeros((n_units, n_features + 6))
    for k, row in enumerate(raw.values):
        temp[k, 0] = int(k // height)
        temp[k, 1] = int(k % width)
        temp[k, 2] = np.nan
        temp[k, 3] = np.nan
        temp[k, 4] = np.nan
        temp[k, 5] = np.nan
        temp[k, 6:] = row

    df = pd.DataFrame(temp, columns=['I', 'J', 'TARGET', 'Q_ERROR', 'BMU_DIST', 'N_HITS'] + feature_names)
    df.index.name = 'ID'

    return df


if __name__ == '__main__':
    import sys
    import pandas as pd

    som_path = sys.argv[1]
    feature_names_path = sys.argv[2]
    output_path = sys.argv[3]

    with open(feature_names_path, 'r') as f: feature_names = f.read().splitlines()
    df = load_unitmap(filename=som_path, feature_names=feature_names)
    df.to_csv(output_path)

