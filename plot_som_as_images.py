

def plot_som_as_images(som, feature_names, output_filename):
    import matplotlib.pyplot as plt
    import numpy as np

    # Get the map structure
    map_height, map_width = som['I'].max()+1, som['J'].max()+1  # because indexes
    unit_width = unit_height = int(np.sqrt(len(feature_names)))

    # Plotting parameters
    L = 1  # How big should each subplot be?

    # Create axis grid
    fig, axarr = plt.subplots(int(map_width), int(map_height),
                              figsize=(map_height * L, map_width * L),
                              sharey=True, sharex=True)

    # Iterate over units and plot
    M, m = np.max(som[feature_names].values), np.min(som[feature_names].values)
    for index, row in som.iterrows():

        reshaped = np.reshape(row[feature_names].values, (unit_width, unit_height))

        ax = axarr[row['I'].astype(int), row['J'].astype(int)]
        ax.axis('off')
        ax.imshow(reshaped,
                  vmin=m, vmax=M, cmap='bone_r')

    plt.savefig(output_filename)


if __name__ == "__main__":
    import sys
    import pandas as pd

    # Unpack
    som_path = sys.argv[1]
    feature_names_path = sys.argv[2]
    output_filename = sys.argv[3]

    # Interface with storage
    with open(feature_names_path, 'r') as f: feature_names = f.read().splitlines()
    som = pd.read_csv(som_path, index_col='ID')

    # Execute
    plot_som_as_images(som=som,
                       feature_names=feature_names,
                       output_filename=output_filename)

